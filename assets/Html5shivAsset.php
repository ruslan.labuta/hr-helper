<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/22/15
 * Time: 9:16 PM
 */
namespace app\assets;

use yii\web\AssetBundle;

class Html5shivAsset extends AssetBundle
{
    public $sourcePath = '@bower/html5shiv';
    public $js = [
        'dist/html5shiv.min.js'
    ];

    public $jsOptions = [
        'condition'=>'lt IE 9'
    ];
}
