/**
 * Created by ruslan on 11/23/15.
 */
var searchEmpolyeeUtils = {
    html:'',
    csrfToken:'',
    clickHandler:function(){
        $('body').on('click','.search-employee-button',function(){
            searchEmpolyeeUtils.html = $(this).closest('.search-employee-parent');
            $(this,searchEmpolyeeUtils.html).remove();
            searchEmpolyeeUtils.csrfToken = $('#add-employee .modal-body input[name="_csrf"]').val();
            $('#add-employee').modal();
            $('#add-employee').on('hidden.bs.modal', function () {
                window.location.reload();
            })
        })
        $('#add-employee .add-employee-action').click(function(){
            var self = this;
            searchEmpolyeeUtils.html = $(searchEmpolyeeUtils.html).html();
            $.ajax({
                url:'/category/add-employee-to-category',
                type:'POST',
                dataType:'json',
                beforeSend:function(){
                },
                data:{
                    'html':searchEmpolyeeUtils.html,
                    '_csrf':searchEmpolyeeUtils.csrfToken,
                    'dataId':$('#add-employee .select-category option:selected').val()
                },
                'success':function(answer){
                    $('#add-employee .modal-body').html(answer.message);
                    $(self,'#add-employee').remove();
                }
            })
        })

    },
    init:function(){
        this.clickHandler()
    }
}

$(document).ready(function(){
    searchEmpolyeeUtils.init();
})
