<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/22/15
 * Time: 9:17 PM
 */
namespace app\assets;

use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/font-awesome';
    public $css = [
        'css/font-awesome.min.css'
    ];
}