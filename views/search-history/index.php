<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/8/15
 * Time: 10:03 PM
 */
 // @$searchResults
?>
<?php foreach($searchResults->value as $site):?>
    <?php if($count = count($site)):?>
        <div class="callout callout-warning">
            <h3 class="box-title">Результати пошуку за <?=$searchResults->dateInsert?></h3>

            <p>Кількість на сторінці <?=' ('.$count.')'?> .</p>
        </div>
        <?php foreach($site as $employee):?>
            <div class="box box-success">
                <div class="box-header with-border">
                    <i class="fa fa-bullhorn"></i>
                    <h5 class="box-title"><?=$employee['date']?>
                        <p class="help-block">
                            <?=$employee['siteName']?>
                            <a href="<?=$employee['guid']?>" target="_blank">profile link</a>
                        </p>
                    </h5>
                </div>
                <div class="box-body">
                    <h3><?=$employee['name']?></h3>
                    <dd><?=$employee['description']?></dd>
                    <button type="button" class="btn btn-success"><?=Yii::t('app','Додати')?></button>
                </div>
            </div>
        <?php endforeach;?>
    <?php else: ?>
        <?php
        echo $this->render('empty_result');
        ?>
    <?php endif;?>
<?php endforeach;?>