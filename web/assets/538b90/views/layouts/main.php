<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/22/15
 * Time: 9:54 PM
 */
$this->beginContent('@rlabuta/landingpage/views/layouts/common.php'); ?>
    <div class="box">
        <div class="box-body">
            <?php echo $content ?>
        </div>
    </div>
<?php $this->endContent(); ?>