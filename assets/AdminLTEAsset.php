<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/23/15
 * Time: 9:38 PM
 */
namespace app\assets;
use yii\web\AssetBundle;

class AdminLTEAsset extends AssetBundle
{
    public $sourcePath = '@bower/admin-lte/dist';
    public $js = [
        'js/app.min.js'
    ];
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css'
    ];
}