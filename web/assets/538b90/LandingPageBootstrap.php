<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/26/15
 * Time: 8:58 PM
 */
namespace rlabuta\landingpage;
use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\Url;

class LandingPageBootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app){
        // check if this is not console
        if($app->id == 'console') return;

        // to do refactor this shit (((
        if( \Yii::$app->request->url == '/' ||
            \Yii::$app->request->url == '/site' ||
            \Yii::$app->request->url == '/site/' ||
            \Yii::$app->request->url == '/site/index' ||
            \Yii::$app->request->url == '/site/index/'
        ){
            $app->setViewPath('@rlabuta/landingpage/views');
        }
    }
}
