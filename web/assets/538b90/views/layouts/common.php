<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/22/15
 * Time: 9:55 PM
 */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
    use rlabuta\landingpage\LandingPageAsset;
    $assets = LandingPageAsset::register($this);
$this->beginContent('@rlabuta/landingpage/views/layouts/base.php'); ?>
    <!-- Header -->
    <header id="header">
        <p><a id="top"></a></p>
        <nav id="nav">
            <ul>
                <li><a class="anchor-animate" href="#top">Головна</a></li>
                <li><a class="anchor-animate" href="#middle">Цілі</a></li>
                <li><a class="anchor-animate" href="#contact">Контакти</a></li>
            </ul>
        </nav>
    </header>

    <!-- Banner -->
    <section id="banner">
        <h2>HR-helper</h2>
        <p>Це програма створена для полегшення роботы HR- спеціалістів</p>
        <ul class="actions">
            <li>
                <a href="#" class="button big init-login">Увійти</a>
            </li>
        </ul>
    </section>

    <!-- One -->
    <section id="one" class="wrapper style1 align-center">
        <div class="container">
            <p><a id="middle"></a></p>
            <header>
                <h2>HR-helper створена кваліфікованими спеціалістами</h2>
                <p>Ми розуміємо що потрібно вам</p>
            </header>
            <div class="row 200%">
                <section class="4u 12u$(small)">
                    <i class="icon big rounded fa-clock-o"></i>
                    <p>Наш сервіс допоможе зекономити вам час</p>
                </section>
                <section class="4u 12u$(small)">
                    <i class="icon big rounded fa-comments"></i>
                    <p>Швидка інтеграція с сервісами</p>
                </section>
                <section class="4u$ 12u$(small)">
                    <i class="icon big rounded fa-user"></i>
                    <p>Збереження контактів і налаштування профайлу</p>
                </section>
            </div>
        </div>
    </section>

    <!-- Two -->
    <section id="two" class="wrapper style2 align-center">
        <div class="container">
            <header>
                <h2>HR новини</h2>
                <p></p>
            </header>
            <div class="row">
                <section class="feature 6u 12u$(small)">
                    <img class="image fit" src="<?=$assets->baseUrl ?>/web/img/pic01.jpg" alt="" />
                    <h3 class="title">Треніниги с HR майстерності </h3>
                    <p>Тренери можуть системно проводити навчання у форматі тренінгу, працюючи також у форматі предтренінгової та посттренінгової роботи

                  Учасники розуміють, як можна краще володіти своїми емоціями та страхами; отримують більше задоволенн</p>
                </section>
                <section class="feature 6u$ 12u$(small)">
                    <img class="image fit" src="<?=$assets->baseUrl ?>/web/img/pic03.jpg" alt="" />
                    <h3 class="title">Систематизування в HR - сфері</h3>
                    <p>Щоб систематизувати роботу компанії, чітке розуміння HR- і процесів бізнесу повинне бути у всіх учасників проекту по розвитку компанії. Від HR’ів до топ-менеджерів і власників.</p>
                </section>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <p><a id="contact"></a></p>
            <div class="row" style="margin-left: 214px;">
                <section class="4u 6u(medium) 12u$(small)">
                    <h3>Що потрібно для того щоб скористатися сервісом</h3>
                    <p></p>
                    <ul class="alt">
                        <li><a href="#">Напишіть нам контактний емєйл</a></li>
                        <li><a href="#">Потім наші менеджери звяжуться з вами </a></li>
                        <li><a href="#">Після вам буде надано доступ до панелі</a></li>
                    </ul>
                </section>
                <section class="4u$ 12u$(medium) 12u$(small)">
                    <h3>Contact Us</h3>
                    <ul class="icons">
                        <li><a href="#" class="icon rounded fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon rounded fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon rounded fa-pinterest"><span class="label">Pinterest</span></a></li>
                        <li><a href="#" class="icon rounded fa-google-plus"><span class="label">Google+</span></a></li>
                        <li><a href="#" class="icon rounded fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    </ul>
                    <ul class="tabular">
                        <li>
                            <h3>Address</h3>
                            1234 Київ<br>
                            Львівска, TN 00000
                        </li>
                        <li>
                            <h3>Mail</h3>
                            <a href="#">admin@env.in.ua</a>
                        </li>
                        <li>
                            <h3>Phone</h3>
                            (000) 000-0000
                        </li>
                    </ul>
                </section>
            </div>
            <ul class="copyright">
                <li>&copy; Untitled. All rights reserved.</li>
                <li>Design: <a href="http://templated.co">TEMPLATED</a></li>
                <li>Images: <a href="http://unsplash.com">Unsplash</a></li>
            </ul>
        </div>
    </footer>

<?php $this->endContent(); ?>