<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/13/15
 * Time: 9:59 PM
 */
namespace app\assets;

use yii\web\AssetBundle;

class SlimScrollAsset extends AssetBundle{
    public $sourcePath = '@bower/admin-lte/plugins/slimScroll';
    public $js = [
        'jquery.slimscroll.min.js'
    ];
}