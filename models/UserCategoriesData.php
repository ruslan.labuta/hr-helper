<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/14/15
 * Time: 10:18 PM
 */
namespace app\models;
use yii\db\ActiveRecord;

class UserCategoriesData extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(){
        return  '{{%userCategoriesData}}';
    }

}