/**
 * Created by ruslan on 12/3/15.
 */
var searchEmployee ={
    csrfToken:'',
    submitButton:'#search-btn',
    search:function(){
        $(searchEmployee.submitButton).on('click',function(event){
            event.preventDefault();
            $.ajax({
                url:'/search-employee/search',
                type:'POST',
                dataType:'json',
                beforeSend:function(){
                    var loader = '<div class="cssload-thecube">';
                      loader += '<div class="cssload-cube cssload-c1"></div>';
                      loader +=  '<div class="cssload-cube cssload-c2"></div>';
                      loader +=   '<div class="cssload-cube cssload-c4"></div>';
                      loader +=   '<div class="cssload-cube cssload-c3"></div></div>';

                    $('.site-index .left-block .search-result').html(loader);
                },
                data:{
                    'searchPhrase':$('.search-phrase').val(),
                    '_csrf':searchEmployee.csrfToken
                },
                'success':function(answer){
                    $('.cssload-thecube').remove();
                    $('.site-index .left-block .search-result').html(answer.html);
                }
            })
        })
    },
    init:function(){
        searchEmployee.search();
    }
}