<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/14/15
 * Time: 8:19 PM
 */
use yii\helpers\Html;
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Категорії</h3>
    </div>
    <?php if($flashMessage = \Yii::$app->getSession()->getFlash('add-category')):?>
        <div class="callout callout-<?=$flashMessage['status']?>">
            <h4><?=$flashMessage['message']?></h4>
        </div>
    <?php \Yii::$app->getSession()->destroySession('add-category'); endif;?>
    <?= Html::beginForm('/category','post',['class'=>'add-category-form']);?>
    <div class="input-group input-group-sm">
        <input type="text" class="form-control" name="category-name">
        <span class="input-group-btn">
          <button type="submit" class="btn btn-info btn-flat">Додати категорію</button>
        </span>
    </div>
    <?= Html::endForm();?>

    <?php if(count($categories)):?>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="table table-condensed">
            <tbody><tr>
                <th style="width: 10px">#</th>
                <th>Назва категорії</th>
                <th>Кількість кандидатів</th>
            </tr>
            <?php $c = 1;foreach($categories as $cat):?>
                <tr>
                    <td><?=$c; $c++;?></td>
                    <td>  <a href="/category/show/<?=$cat['categoryId']?>"><?=$cat['categoryName']?></a></td>
                    <td><span class="badge bg-red"><?=$cat['candidates']?></span></td>
                </tr>
            <?php endforeach;?>
            </tbody></table>
    </div>
    <!-- /.box-body -->
    <?php else:?>
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Мої категорії</h4>
            Вам потрібно додати хоча б одну категорію
        </div>

    <?php endif?>
</div>