<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/14/15
 * Time: 6:47 PM
 */
namespace app\models;
use yii\db\ActiveRecord;

class Categories extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(){
        return  '{{%categories}}';
    }

}