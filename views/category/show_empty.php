<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/14/15
 * Time: 8:47 PM
 */?>
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i> Вам потрібно додати хоча б одного кандидата до категорії</h4>
</div>