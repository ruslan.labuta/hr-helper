<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/12/15
 * Time: 10:40 AM
 */

namespace app\models;
use yii\db\ActiveRecord;

class SearchHistoryModel extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(){
       return  '{{%searchHistory}}';
    }


}
