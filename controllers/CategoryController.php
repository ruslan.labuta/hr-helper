<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/14/15
 * Time: 6:40 PM
 */
namespace app\controllers;

use yii\base\Controller;
use app\models\Categories;
use app\models\UserCategories;
use app\models\UserCategoriesData;

/**
 * Class CategoryController
 * @package app\controllers
 */
class CategoryController extends Controller{

    /**
     * @return string
     */
    public function actionIndex(){
        $profile = \Yii::$app->getUser()->identity->profile;
        if($categoryName = \Yii::$app->request->post('category-name')){
            if(!Categories::find()->where(['categoryName' => $categoryName])->one()){
                $category = new Categories();
                $category->categoryName = $categoryName;
                $category->dateInserted = date('Y-m-d H:m:s');
                $category->insert();
                $userCategory = new UserCategories();
                $userCategory->userId = $profile->user->id;
                $userCategory->categoryId = $category->id;
                $userCategory->insert();
                \Yii::$app->getSession()->setFlash('add-category',[
                    'status' => 'success',
                    'message'=> 'Категорія '.$categoryName.' успішно створена'
                ]);
            }else{
                \Yii::$app->getSession()->setFlash('add-category',[
                    'status' => 'warning',
                    'message' => 'Категорія '.$categoryName.' вже існує '
                ]);
            }
        }
        $sql = 'select *,(select count(1) from userCategoriesData
        where userCategoriesId = uC.userCategoriesId) candidates from userCategories1 uC
                            inner join categories c on uC.categoryId = c.id
                            where uC.userId = '.$profile->user->id.' group by uC.userId, uC.categoryId';
        $result = \Yii::$app->db->createCommand($sql)->queryAll();
        return $this->render('categories',['categories' => $result]);
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionShow(){
        $categoryId = \Yii::$app->request->get('id');
        if(!$categoryId){
            throw new \yii\web\NotFoundHttpException();
        }

        $sql = 'select * from userCategories1 uC
                inner join userCategoriesData ucd on ucd.userCategoriesId = uC.userCategoriesId
                where categoryId = '.$categoryId.'  and userId = '.\Yii::$app->getUser()->identity->profile->user->id;
        $result = \Yii::$app->db->createCommand($sql)->queryAll();
        if(!count($result)){
            return $this->render('show_empty');
        }
        return $this->render('show',['result'=>$result]);
    }

    public function actionAddEmployeeToCategory(){
        $answer = [
            'status'=>0,
            'message'=> 'Виникла помилка спробуйте пізніше'
        ];
        $data = \Yii::$app->request->post();
        if($data['dataId']){
            $dataModel = new UserCategoriesData;
            $dataModel->userCategoriesId = $data['dataId'];
            $dataModel->value = $data['html'];
            $dataModel->insert();
            exit(json_encode([
                'status'=>1,
                'message'=> 'Кандидата було успішно добавлено'
            ]));
        }else{
           exit(json_encode($answer));
        }
    }

}