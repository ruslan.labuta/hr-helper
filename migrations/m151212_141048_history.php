<?php
use yii\db\Schema;
use yii\db\Migration;

class m151212_141048_history extends Migration
{
    public function up()
    {
        $this->createTable('{{%searchHistory}}', [
            'id' => Schema::TYPE_PK,
            'userId' => Schema::TYPE_INTEGER . ' NOT NULL',
            'value' => Schema::TYPE_BINARY,
            'dateInsert' => Schema::TYPE_TIMESTAMP. " NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP",
        ]);
/*        $this->addForeignKey(
            'fk_search_user_history',
            '{{%searchHistory}}',
            'userId',
            '{{%user}}',
            'id',
            'cascade',
            'cascade'
        );*/

    }

    public function down()
    {
/*        $this->dropForeignKey(
            'fk_search_user_history',
            '{{%searchHistory}}'
        );*/

        $this->dropTable('{{%searchHistory}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
