<div class="box box-default">
    <div class="box-header with-border">
        <i class="fa fa-info"></i>

        <h3 class="box-title">Історія пошуку</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>Ви ще не робили пошук</h4>
        </div>
    </div>
    <!-- /.box-body -->
</div>