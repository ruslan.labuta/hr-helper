<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11/26/15
 * Time: 8:47 PM
 */
namespace rlabuta\landingpage;

use yii\web\AssetBundle;

class LandingPageAsset extends AssetBundle
{
    public $sourcePath = '@rlabuta/landingpage';
    public $js = [
        'web/js/jquery.min.js',
        'web/js/skel.min.js',
        'web/js/skel-layers.min.js',
        'web/js/jquery.cookie.js',
        'web/js/init.js',
    ];
    public $css = [
        'web/css/skel.css',
        'web/css/style.css',
        'web/css/style-xlarge.css',
    ];
}