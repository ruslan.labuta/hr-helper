<?php

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
use yii\helpers\Html;
?>
<div class="site-index">

    <div class="left-block">
        <?php
            echo rlabuta\searchemployee\widgets\SearchEmployeeWidget::widget();
        ?>
    </div>
    <div class="right-block">
        <aside class="control-sidebar control-sidebar-dark control-sidebar-open" style="position: fixed; max-height: 100%; overflow: auto; padding-bottom: 50px;">
            <!-- Create the tabs -->
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div class="tab-pane active" id="control-sidebar-home-tab">
                    <h3 class="control-sidebar-heading">Фільтри для пошуку</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript::;">
                                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Robota.ua</h4>

                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript::;">
                                <i class="menu-icon fa fa-user bg-yellow"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Work.ua</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript::;">
                                <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">JOB.ukr.net</h4>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript::;">
                                <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                <div class="menu-info">
                                    <h4 class="control-sidebar-subheading">Novarobota.ua</h4>

                                    <p>Execution time 5 seconds</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /.control-sidebar-menu -->

<!--                    <h3 class="control-sidebar-heading">Tasks Progress</h3>
                    <ul class="control-sidebar-menu">
                        <li>
                            <a href="javascript::;">
                                <h4 class="control-sidebar-subheading">
                                    Custom Template Design
                                    <span class="label label-danger pull-right">70%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript::;">
                                <h4 class="control-sidebar-subheading">
                                    Update Resume
                                    <span class="label label-success pull-right">95%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript::;">
                                <h4 class="control-sidebar-subheading">
                                    Laravel Integration
                                    <span class="label label-warning pull-right">50%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript::;">
                                <h4 class="control-sidebar-subheading">
                                    Back End Framework
                                    <span class="label label-primary pull-right">68%</span>
                                </h4>

                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                </div>
                            </a>
                        </li>
                    </ul>-->
                    <!-- /.control-sidebar-menu -->

                </div>
                <!-- /.tab-pane -->
                <!-- /.tab-pane -->
            </div>
        </aside>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="add-employee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Опціональне вікно</h4>
            </div>

            <div class="modal-body">
                <?php
                    $profile = Yii::$app->getUser()->identity->profile;
                    $sql = 'select *,(select count(1) from userCategoriesData where userCategoriesDataId = uC.userCategoriesId) candidates from userCategories1 uC
                                    inner join categories c on uC.categoryId = c.id
                                    where uC.userId = '.$profile->user->id.' group by uC.userId, uC.categoryId';
                    $result = \Yii::$app->db->createCommand($sql)->queryAll();
                ?>
                <?php if(count($result)):?>
                <?=Html::beginForm();?>
                <div class="form-group">
                    <label>Вибрати категорію</label>
                    <select class="form-control select-category">
                        <?php foreach($result as $res):?>
                         <option value="<?=$res['userCategoriesId']?>"><?=$res['categoryName']?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <?=Html::endForm();?>
               <?php else:?>
                    Вам потрібно додати хочаб одну категорію
                <?php endif;?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php if(count($result)):?>
                <button type="button" class="btn btn-primary  add-employee-action">Save changes</button>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
