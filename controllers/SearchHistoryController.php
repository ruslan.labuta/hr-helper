<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12/8/15
 * Time: 10:02 PM
 */
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\SearchHistoryModel;

/**
 * Class SearchSiteController
 * @package app\controllers
 */
class SearchHistoryController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchResults = SearchHistoryModel::find()
            ->where(['userId' => \Yii::$app->getUser()->identity->profile->user->id])->one();
        if(isset($searchResults->value)){
            $searchResults->value = unserialize($searchResults->value);
            return $this->render('index',[
                'searchResults' => $searchResults,
            ]);

        }else{
            return $this->render('empty_result');
        }
    }

}